=== Posts From Tag ===
Contributors: jorge-vitrubio
Tags: posts from tag, tag listing, post list from tag, widget, plugin, post listing widget, plugin to list posts, list posts from specific tag, post, sidebar, post from tag, list tag post, tag posts
Version: 1.0.0
Requires at least: 5.0
Tested up to: 6.0
Requires PHP: 5.6.20
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin is a fork of [Post From Category](https://wordpress.org/plugins/posts-from-category/) by [Manesh Timilsina](https://profiles.wordpress.org/maneshtimilsina/).

Plugin to display posts from specific tag. It comes with multiple layout option to display post list. Option to select tag or exclude post is available which provide advance filter to your requirement. You can enable or disable posted date, thumbnail, read more button and more easily from widget.

Following features are offered by plugin:

* Multiple layout options to list posts
* Title field for your listing
* Select tag from dropdown
* Order posts by author, title, id, date, number of comments, menu order or random
* Order posts in Ascending or Descending order
* Select number of posts to display
* Exclude posts from listing
* Change word length of excerpt
* Enable/Disable featured image
* Select size of featured image
* Enable/Disable publish date
* Enable/Disable read more button and its text

= Useage of Shortcode: =

You can use shortcode to display your posts in the content area of your website. It is supported by Gutenberg Block, Page Builders like Elementor and more.

<pre>[pft layout="layout-one" tag="0" order_by="date" order="DESC" post_number="5" length="10" readmore="Read More" show_date="true" show_image="true" image_size="full"]</pre>

Parameters supported by this plugin are explained below:

* <strong>layout: </strong> Supports 2 layouts (layout-one and layout-two)
* <strong>tag: </strong> Tag ID
* <strong>order_by: </strong> Order your posts by author, title, ID, date, menu_order, comment_count, rand
* <strong>order: </strong> DESC or ASC
* <strong>post_number: </strong> Number of posts to show
* <strong>length: </strong> Length of excerpt. Set 0 to hide excerpt
* <strong>readmore: </strong> Read More text
* <strong>show_date: </strong> true or false
* <strong>show_image: </strong> true or false
* <strong>image_size: </strong> Supports thumbnail, medium, large, full or any size defined by the theme

= Other Useful Plugins =

- [Post from Tag](https://wordpress.org/plugins/advanced-google-recaptcha/)


== Installation ==

1. Download the plugin from plugin directory

2. Extract the file contents

3. Upload the directory to your WordPress plugins' directory ( /wp-content/plugins/ )

4. Activate the plugin from the WordPress Dashboard

5. Go to Widgets, add 'Post From Tag' widget to your desire sidebar

6. Fill in the desired fields and we're good to go

== Frequently Asked Questions ==

= Does this plugin provide shortcode ? =

Yes, you can find PFT shortcode icon at the top bar of WP Editor. Click on it to insert shortcode.

= Can I use this plugin using Gutenberg block or Elementor widget ? =

Yes, you can use the shortcode block of Gutenberg (and shortcode in Elementor widget) to show your posts.

= Where can I find setting for plugin? =

This plugin do not have separate setting page. You can provide necessary details in widgets.


== Screenshots ==

1. Backend View
2. Frontend View

== Changelog ==

= 1.0.0 =
- First publicly distributable version
