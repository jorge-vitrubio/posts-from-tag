<?php
//add admin page
add_action( 'admin_menu', 'pft_admin_menu' );

function pft_admin_menu() {
	add_menu_page(
        esc_html__( 'Posts From Tag', 'PFT' ),
        esc_html__( 'Posts From Tag', 'PFT' ),
        'manage_options',
        'pft-options',
        'pft_options_page',
        'dashicons-editor-ul'
    );
}

function pft_options_page(){

	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( esc_html__( 'You do not have sufficient permissions to access this page.', 'PFT' ) );
	}
	?>

	<div class="wrap about-wrap theme-info-wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

		<div class="about-text">
			<?php echo esc_html__( 'Plugin to display posts from a specific tag.', 'PFT' ); ?>
		</div>

		<p>
			<a href="https://wordpress.org/support/plugin/posts-from-tag/" class="button button-primary" target="_blank"><?php echo esc_html__( 'Support', 'PFT' ); ?></a>
			<a href="https://wordpress.org/support/plugin/posts-from-tag/reviews/" class="button" target="_blank"><?php echo esc_html__( 'Leave a Review', 'PFT' ); ?></a>
			<a href="https://vitrubio.net" class="button" target="_blank"><?php echo esc_html__( 'Request Customization', 'PFT' ); ?></a>
		</p>

		<div class="feature-section has-2-columns alignleft">
			<div class="card">
				<h3><?php echo esc_html__( 'How to Use (Using Widget)', 'PFT' ); ?></h3>
				<ol>
					<li><?php echo esc_html__( 'Go to Appearance >> Widgets', 'PFT' ); ?></li>
					<li><?php echo esc_html__( 'Find: Posts From Tag widget', 'PFT' ); ?></li>
					<li><?php echo esc_html__( 'Add the widget to the sidebar you want to use', 'PFT' ); ?></li>
					<li><?php echo esc_html__( 'Fill in the desired fields and we are good to go', 'PFT' ); ?></li>
				</ol>
			</div>

			<div class="card">
				<h3><?php echo esc_html__( 'How to Use (Using Shortcode)', 'PFT' ); ?></h3>
				<code>[pft layout="layout-one" tag="0" order_by="date" order="DESC" post_number="5" length="10" readmore="Read More" show_date="true" show_image="true" image_size="full"]</code>
				<p><a href="https://wordpress.org/plugins/posts-from-tag/" target="_blank"><?php echo esc_html__( 'Click here to see shortcode parameters', 'PFT' ); ?></a></p>
			</div>

			<div class="card">
				<h3><?php echo esc_html__( 'Recommended Plugins', 'PFT' ); ?></h3>
				<ol>
					<li><a href="https://wordpress.org/plugins/post-from-category/" target="_blank"><?php echo esc_html__( 'Post From Category', 'PFT' ); ?></a></li>
				</ol>
			</div>

		</div>

	</div>
	<?php
}
